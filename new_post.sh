#!/bin/bash

NAME=_drafts/$1.markdown
touch $NAME

p(){
    echo $@ >> $NAME
}

p ---
p title: $1
p date: $(date -Is | awk 'gsub("T", " ")' | awk 'gsub("+", " +")') 
p categories: 
p path: $1
p ---
p

echo $NAME
